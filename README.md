# Introduction
An end-to-end practical example illustrating the different platforms and patterns I'm experienced with.

# Getting Started
The project requries you have Visual Studio and Node Js installed and aren't blocked from updating packages see [Alternative Source]
1.	Clone or Download source
2.  Restore NuGet Packages
3.	Build
3.	Start cmd in Ui.SmithReview and run npm update
4.  Run Webpack-Sass-Ui.SmithReview.bat in the root directory
5.  Press *Start* in Visual Studio, a local API endpoint and front-end application should launch

# Build and Test
There are a few example C# unit-tests in the test project, and the biginnings of a karma config
